import numpy as np
from numpy.fft import fft2
from numpy.fft import ifft2
from numpy.fft import fftshift
from PIL import Image
import matplotlib
matplotlib.use('TkAgg')
from matplotlib import pyplot as plt

# Bild als array abspeichern
Image = Image.open("Bild2.png")
imgGray = Image.convert('L')
array_image = np.array(imgGray)

# Bild vom Amplitudenspektrum erstellen
f = fft2(array_image)
# Umsortieren
fshift = fftshift(f)
FT = np.abs(fshift)
FT_log = np.log(np.abs(fshift))  # mit log() wird der Kontrast angehoben
plt.figure(figsize=(12, 12))
plt.subplot(121)
plt.xticks([]), plt.yticks([])
plt.imshow(array_image, cmap = 'gray')
plt.title('Referenzbild im Ortsraum')
plt.subplot(122)
plt.xticks([]), plt.yticks([])
plt.imshow(FT_log, cmap=plt.cm.gray)
plt.title('Fourier-Transformierte vom Referenzbild')
plt.show()