import numpy as np
from numpy.fft import fft2
from numpy.fft import ifft2
from PIL import Image
import matplotlib
matplotlib.use('TkAgg')
from matplotlib import pyplot as plt


# Bilder als array abspeichern
Image1 = Image.open("Bild1.png")  # Alternativ Bild3.png verwenden
imgGray1 = Image1.convert('L')
Image2 = Image.open("Bild2.png")
imgGray2 = Image2.convert('L')
array_image1 = np.array(imgGray1)
array_image2 = np.array(imgGray2)
Bild1 = array_image1
Bild2 = array_image2

# Masken definieren (schwarzer Hintergrund soll bei beiden Bildern ignoriert werden)
m1 = (array_image1 != 0)
m2 = (array_image2 != 0)
# Bei keiner Maskierung die nachfolgenden Einser-Masken verwenden
#m1 = np.ones_like(array_image1)
#m2 = np.ones_like(array_image2)

array_image1 = np.multiply(array_image1, m1)
array_image2 = np.multiply(array_image2, m2)

# Zero-Padding + Int zu Float
AnzahlZeilen1, AnzahlSpalten1 = array_image1.shape
AnzahlZeilen2, AnzahlSpalten2 = array_image2.shape
array_image1 = np.array(np.pad(array_image1, ((0, AnzahlZeilen2-1), (0, AnzahlSpalten2-1)), 'constant'), np.float64())
array_image2 = np.array(np.pad(array_image2, ((0, AnzahlZeilen1-1), (0, AnzahlSpalten1-1)), 'constant'), np.float64())
m1 = np.array(np.pad(m1, ((0, AnzahlZeilen2-1), (0, AnzahlSpalten2-1)), 'constant'), np.float64())
m2 = np.array(np.pad(m2, ((0, AnzahlZeilen1-1), (0, AnzahlSpalten1-1)), 'constant'), np.float64())

# Templatebild und Templatemaske um 180 grad drehen
array_image2_rot = np.rot90(np.rot90(array_image2))
m2_rot = np.rot90(np.rot90(m2))

# FFTs und inverse FFTs für NCC bestimmen
F1 = fft2(array_image1)
F2 = fft2(array_image2_rot)
M1 = fft2(m1)
M2 = fft2(m2_rot)
x1 = np.round(ifft2(np.multiply(M1, M2)))

# Einträge dürfen nicht Null sein (da Summe im Nenner)
x1[x1 <= 0.01] = 0.1

x2 = ifft2(np.multiply(F1, M2))
x3 = ifft2(np.multiply(M1, F2))
x4 = ifft2(np.multiply(F1, F2))
x5 = fft2(np.multiply(array_image1, array_image1))
x6 = ifft2(np.multiply(x5, M2))
x7 = fft2(np.multiply(array_image2_rot, array_image2_rot))
x8 = ifft2(np.multiply(M1, x7))

num = x4 - np.divide(np.multiply(x2, x3), x1)
den1 = x6 - np.divide(np.multiply(x2, x2), x1)
den2 = x8 - np.divide(np.multiply(x3, x3), x1)
den1[den1 <= 0] = 10000000
den2[den2 <= 0] = 10000000
den = np.sqrt(np.multiply(den1, den2))
NCC = num / den

# NCC begrenzen auf [-1,1], Ausreißer existieren, bei zu kleinen Überlappungsbereich
np.clip(NCC, a_min=-1, a_max=1, out=NCC)

# Schwellenwert für minimale Pixelanzahl im Überlappungsbereich einführen
NCC[x1 < 0.2 * np.max(x1)] = 0.0
NCC = np.real(NCC)
index = np.unravel_index(np.argmax(NCC, axis=None), NCC.shape)
shift = -(index - np.array(array_image2.shape) + 1)
print('Translationsvektor: [u,v] =', shift)
print('Maximaler NCC =', NCC[index[0]][index[1]])

# Registrierungsergebnis
for i in range(0, len(Bild1)):
    for j in range(0, len(Bild1[0])):
        Bild2[i + shift[0]][j + shift[1]] = Bild1[i][j]

plt.imshow(Bild2, cmap = 'gray')
plt.show()



